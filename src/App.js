import React, { Component } from "react";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
      error: false
    };
  }

  changeCounter(value) {
    if (this.state.counter + value >= 0)
      this.setState({ counter: this.state.counter + value, error: false });
    else{
      this.setState({error: true})
    }
  }
  render() {
    return (
      <div data-test="component-app">
        <h1 data-test="counter-display">
          The counter is currently {this.state.counter}
        </h1>
        {this.state.error ? (
          <h3 data-test="error-message">The counter cannot go below zero!</h3>
        ) : (
          ""
        )}
        <button
          onClick={() => this.changeCounter(1)}
          data-test="increment-button"
        >
          Increment Counter
        </button>
        <button
          onClick={() => this.changeCounter(-1)}
          data-test="decrement-button"
        >
          Decrement Counter
        </button>
      </div>
    );
  }
}

export default App;
